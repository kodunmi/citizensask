<?php

namespace App\Http\Controllers;

use App\Template;
use App\Download;
use App\Category;
use App\Experience;
use App\User;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['downloadTemplate','dashboard','updateProfile']);
    }
    public function index()
    {
        $users = User::all();
        $downloads = Download::all();
        $numbersOfDownloads = $downloads->count();
        $numbersOfUsers = $users->count();
        $exps = Experience::all();
        $templates = Template::all();
        return response()->view('citizenask.pages.home', compact('exps','templates','numbersOfUsers','numbersOfDownloads'));
    }

    public function contact()
    {
        return response()->view('citizenask.pages.contact');
    }
    public function about()
    {
        return response()->view('citizenask.pages.about');
    }
    public function howToAsk()
    {
        return response()->view('citizenask.pages.how-to-ask');
    }
    public function templates(Request $request)
    {
        $templates = Template::paginate(12);
        $cats = Category::all();
        return response()->view('citizenask.pages.templates', compact('templates', 'cats'));
    }

    public function showTemplate($id)
    {
        $template = Template::find($id);
        return response()->view('citizenask.pages.template', compact('template'));
    }
    public function experience()
    {
       $exps = Experience::paginate(20);
        $cats = Category::all();
        return response()->view('citizenask.pages.experience', compact('exps','cats'));
    }

    public function dashboard()
    {
        $exps = Experience::paginate(20);
        $cats = Category::all();
        return response()->view('citizenask.pages.dashboard',compact('exps','cats'));
    }

    public function downloadTemplate($template_id)
    {
        
        $downloads = new Download;
        $downloads->user_id = auth()->id();
        $downloads->template_id = $template_id;
        $downloads->user_score = 5;
        $downloads->save();

        $tmp = Template::find($template_id);
        $file = public_path()."/admin/templates/".$tmp->letter;

        $headers = [
            'Content-Type' => 'application/pdf',
            'Cache-Control' => 'max-age=86400',
        ];

        return response()->download($file, $tmp->letter, $headers);
        
    }

    public function showCategory($id){
        $templates = Template::where('category_id', $id)->paginate(1);
        $cats = Category::all();
        return response()->view('citizenask.pages.templates', compact('templates', 'cats'));
        
    }

    public function getTemplates($id){
        $templates = Template::where('category_id', $id)->get();
        return response()->json(['tmps' => $templates ]);
    }

    public function updateProfile(Request $request, $id){

        $user = User::find($id);
        $user->name = $request->name;
        $user->country = $request->country;
        $user->state = $request->state;
        $user->age_range = $request->age_range;
        $user->local_government = $request->local_government;
        $user->email = $request->email;
        $user->save();

       return response()->json(['message' => 'profile updated successfully']);
    }

    public function viewProfile($user_name,$id){
        $user = User::find($id);
        return view('citizenask.pages.user', compact('user'));
    }
}
