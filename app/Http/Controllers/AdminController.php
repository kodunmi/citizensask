<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function profile(){
        return view('admin.pages.profile');
    }
   
    public function profileUpdate(Request $request, $id){

        $admin = Admin::find($id);
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->department = $request->department;
        $admin->position = $request->position;
        $admin->save();

        $request->session()->flash('alert-type', ' alert-success');
        return redirect()->back()->with('message', 'Profile updated');

    }  
}
