<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Image;
use Illuminate\Support\Facades\Session;

class SessionController extends Controller
{
    public function __construct() {
        $this->middleware('guest:admin')->except('logout');
    }

    //this shows the admin registration form
    public function showRegistrationForm(){

        return view('admin.pages.register');
    }
    // this method validates and create a new admin
    public function register(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:admins,email',
            'password' => 'required|confirmed',
            'department' => 'required',
            'position' => 'required',
            'image' => 'image|required|mimes:jpg,jpeg,png'
        ]);
        
        $image = $request->file('image');
        $image_name = time().'.'.$image->getClientOriginalExtension();
        $path = public_path()."/admin/images/profile/";
        Image::make($image)->resize(200,200)->save($path.$image_name);

        $admin = new Admin;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->department = $request->department;
        $admin->position = $request->position;
        $admin->image = $image_name;
        $admin->password = Hash::make($request->password);
        $admin->save();

        //this authenticate, login and redirect admin to dashboard
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {

            return redirect()->route('admin.dashboard');
        }

    }

    //this method shows the login form
    public function showLoginForm(){

        return view('admin.pages.login');
    }

    //this method will login the admin
    public function login(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $remember_me = $request->has('remember_me') ? true : false;
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password],$remember_me)) {
            $request->session()->flash('message', 'welcome back');
            return redirect()->route('admin.dashboard');
        }

        // Authentication failed, redirect back to the login form
        return redirect()->back()->withErrors("we don't have your record! Please Check Your Credentials")->withInput( $request->only('email', 'remember') );
    }

    //this method will logout the authenticated user
    public function logout(Request $request)
    {
         Auth::guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route( 'admin.login' ));
    }
   
    
}
