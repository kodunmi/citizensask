<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/citizenaskk';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

       
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'country' => ['required', 'string', 'max:255'],
            'state' => ['required', 'string', 'max:255'],
            'local_government' => ['required', 'string', 'max:255'],
            'about_me' => ['required', 'string', 'max:255'],
            'age_range' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'profile_image' => 'image|mimes:jpg,jpeg,png',
            'phone' => 'required|numeric',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if (!empty(Input::file('profile_image'))) {
            $image = Input::file('profile_image');
            $image_name = time().'.'.$image->getClientOriginalExtension();

            $large_path = public_path()."/users/images/large/";
            $medium_path = public_path()."/users/images/medium/";
            $small_path = public_path()."/users/images/small/";

            Image::make($image)->resize(150,150)->save($large_path.$image_name);
            Image::make($image)->resize(80,80)->save($medium_path.$image_name);
            Image::make($image)->resize(20,20)->save($small_path.$image_name);
        }
        return User::create([
            'name' => $data['name'],
            'country' => $data['country'],
            'state' => $data['state'],
            'local_government' => $data['local_government'],
            'age_range' => $data['age_range'],
            'email' => $data['email'],
            'about_me' => $data['about_me'],
            'phone' => $data['phone'],
            'profile_image' => !empty(Input::file('profile_image')) ? $image_name : '',
            'password' => Hash::make($data['password']),
        ]);
    }
}
