<?php

namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function showCatForm(){
        return view('admin.pages.add-category');
    }

    public function addCategory(Request $request){
        $request->validate([
            'category_name' => 'required' 
        ]);

        $cat = new Category;
        $cat->name = $request->category_name;
        $cat->save();
        $request->session()->flash('alert-type', 'success');
        return back()->with('message', 'category added successfully');
    }

    public function viewCategory(){
        $cats = Category::all();
        return view('admin.pages.view-category', compact('cats'));
    }

    public function editCategory(Request $request, $id){
        $cat = Category::find($id);
        $cat->name = $request->category_name;
        $cat->save();
        $request->session()->flash('alert-type', 'success');
        return back()->with('message', 'category edited successfully');
    }

    public function deleteCategory(Request $request,$id){
        Category::find($id)->delete();
        $request->session()->flash('alert-type', 'success');
        return back()->with('message', 'category deleted successfully');
    }

}
