<?php

namespace App\Http\Controllers;
use App\Category;
use App\Template;
use Image;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function showTmpForm(){
        $cats = Category::all();
        return view('admin.pages.add-template', compact('cats'));
    }
    public function addTemplate(Request $request){
        $request->validate([
            'category' => 'required',
            'title' => 'required',
            'template_image' => 'required|image|mimes:jpg,jpeg,png',
            'template' => 'required|mimes:doc,docx,zip',
            'description' => 'required'
        ]);

        if($request->hasFile('template') && $request->hasFile('template_image')){

            //save the template image into large medium and small folder
            $image = $request->file('template_image');
            $image_name = time().'.'.$image->getClientOriginalExtension();

            $large_path = public_path()."/admin/images/templates/large/";
            $medium_path = public_path()."/admin/images/templates/medium/";
            $small_path = public_path()."/admin/images/templates/small/";

            Image::make($image)->resize(500,700)->save($large_path.$image_name);
            Image::make($image)->resize(300,400)->save($medium_path.$image_name);
            Image::make($image)->resize(50,70)->save($small_path.$image_name);

          
            

            
        

             //save the document
             $template = $request->file('template');
             $template_name = time().'.'.$template->getClientOriginalExtension();
             $path = public_path()."/admin/templates/";
             $template->move($path, $template_name);

             //save into database
             $tmp = new Template;
             $tmp->category_id = $request->category;
             $tmp->description = $request->description;
             $tmp->image = $image_name;
             $tmp->letter = $template_name;
             $tmp->title = $request->title;
             $tmp->save();
             $request->session()->flash('alert-type', 'success');
             return back()->with('message', 'Template Added Successfully');
        }
    }

    public function viewTemplate(){
        $templates = Template::all();
        $cats = Category::all();
        return view('admin.pages.view-templates', compact('templates','cats'));
    }

    public function editTemplate(Request $request, $id){
        $tmp = Template::find($id);
        $tmp->category_id = $request->category;
        $tmp->description = $request->description;
        $tmp->title = $request->title;
        $tmp->save();
        $request->session()->flash('alert-type', 'success');
        return back()->with('message', 'Template Updated Successfully');
    }

    public function deleteTemplate(Request $request, $id){
        Template::find($id)->delete();
        $request->session()->flash('alert-type', 'success');
        return back()->with('message', 'Template Updated Successfully');
    }
}
