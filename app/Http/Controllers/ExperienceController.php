<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Experience;
use App\Category;
class ExperienceController extends Controller
{
    public function storeExperience(Request $request){
        $request->validate([
            'category_id' => 'required',
            'template_id' => 'required',
            'title' => 'required',
            'body' => 'required',

        ]);

        $exp = new Experience;
        $exp->template_id = $request->template_id;
        $exp->user_id = auth()->id();
        $exp->title = $request->title;
        $exp->body = $request->body;
        $exp->save();

        return back()->with('message', ' experience shared succcessfully | we are glade you could make impact');
    }

    public function showExperience($id){
        $exp = Experience::find($id);
        $cats = Category::all();
        return view('citizenask.pages.show-experience', compact('exp','cats'));
    }
}
