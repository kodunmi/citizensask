<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Experience;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comment = new Comment;
        $comment->body = $request->comment_body;
        $comment->user()->associate($request->user());
        $exp = Experience::find($request->exp_id);
        $exp->comments()->save($comment);

        return back();
    }

    public function replyStore(Request $request)
    {
        $reply = new Comment();
        $reply->body = $request->comment_body;
        $reply->user()->associate($request->user());
        $reply->parent_id = $request->get('comment_id');
        $exp = Experience::find($request->exp_id);

        $exp->comments()->save($reply);

        return back();

    }
}
