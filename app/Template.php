<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function experiences(){
        return $this->hasMany('App\Experience');
    }
}
