<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function template(){
        return $this->belongsTo('App\Template');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }
}
