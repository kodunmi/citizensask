<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        view()->composer('citizenask.layout.footer', function($view)
        {

        $view->with('users', \App\User::all()); // you can pass array here aswell

        });
    }
}
