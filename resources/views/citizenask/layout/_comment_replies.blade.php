@foreach ($comments as $comment)
<ul class="comment--items nav">
    <li>
        <!-- Comment Item Start -->
        <div class="comment--item mr--15 clearfix">
            <div class="img float--left" data-overlay="0.3" data-overlay-color="primary">
                @if (empty($comment->user->profile_image))
                    <img src="https://www.gravatar.com/avatar/{{hash('md5',$comment->user->email)}}?s=80&d=wavatar" class="profile-circle"/>
                @else
                    <img src="/users/images/medium/{{$comment->user->profile_image}}" alt="" class="profile-circle"/>
                @endif              
            </div>
            <div class="info ov--h">
                <div class="header clearfix">
                    <div class="meta float--left">
                        <p class="fs--14 fw--700 text-darkest">
                            <a href="#">{{$comment->user->name}}</a>
                        </p>

                        <p>
                            <i class="mr--10 fa fa-clock-o"></i>
                            <span>{{$comment->created_at->diffForHumans()}}</span>
                        </p>
                    </div>

                    <div class="reply text-darker float--right">
                        <a data-toggle="modal" data-target="#open-modal{{$comment->id}}" class="btn btn-default">Reply</a>
                    </div>
                </div>
                <div class="content pt--8 fs--14">
                    <p>{{$comment->body}}</p>
                </div>
            </div>
        </div>
        <!-- Comment Item End -->
        @include('citizenask.layout._comment_replies', ['comments' => $comment->replies]) 
    </li>    
</ul>
<div class="modal fade" id="open-modal{{$comment->id}}" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Leave A Reply</h5>
            <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">close</span>
            </button>
        </div>
        <form method="post" action="{{ route('reply.add') }}">
            @csrf
            <div class="modal-body">
                <div class="form-group">
                    <textarea class="form-control" name="comment_body"></textarea>
                    <input type="hidden" name="exp_id" value="{{ $exp->id }}" />
                    <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                @auth
                   <button type="submit" class="btn btn-primary">Replay</button> 
                @endauth
                @guest
                    <a href="login"><button type="submit" class="btn btn-primary">Login To Replay</button> </a>
                @endguest
                
            </div>
        </form>
        </div>
    </div>
</div>
@endforeach


{{-- @foreach($comments as $comment)
    <div class="display-comment">
        <strong>{{ $comment->user->name }}</strong>
        <p>{{ $comment->body }}</p>
        <a href="" id="reply"></a>
        <form method="post" action="{{ route('reply.add') }}">
            @csrf
            <div class="form-group">
                <input type="text" name="comment_body" class="form-control" />
                <input type="hidden" name="exp_id" value="{{ $exp->id }}" />
                <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-warning" value="Reply" />
            </div>
        </form>
        @include('citizenask.layout._comment_replies', ['comments' => $comment->replies])
    </div>
@endforeach --}}