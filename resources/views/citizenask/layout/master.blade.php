<!DOCTYPE html>
<html dir="ltr" lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- ==== Document Title ==== -->
    <title>@yield('title')</title>
    @yield('meta-information')
    <!-- ==== Document Meta ==== -->
    <meta name="author" content="ThemeLooks">
    <meta name="description" content="Multipurpose Social Network HTML5 Template">
    <meta name="keywords"
        content="social media, social network, forum, shop, bootstrap, html5, css3, template, responsive, retina ready">
    <script src="/citizenask/js/vue.js" type="text/javascript"></script>
    {{-- for chatter pckage css --}}
        @yield('css')
    {{-- end chatter css --}}

    <!-- ==== Favicon ==== -->
    <link rel="icon" href="favicon.png" type="image/png">

    <!-- ==== Google Font ==== -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700%7CRoboto:300,400,400i,500,700">

    <!-- ==== Plugins Bundle ==== -->
    <link rel="stylesheet" href="/citizenask/css/plugins.min.css">

    <!-- ==== Main Stylesheet ==== -->
    <link rel="stylesheet" href="/citizenask/css/style.css">

    <!-- ==== Responsive Stylesheet ==== -->
    <link rel="stylesheet" href="/citizenask/css/responsive-style.css">

    <!-- ==== Color Scheme Stylesheet ==== -->
    <!-- Select2 -->
    <link rel="stylesheet" href="../../admin/bower_components/select2/dist/css/select2.min.css">


    <!-- ==== Custom Stylesheet ==== -->
    <link rel="stylesheet" href="/citizenask/css/custom.css">

    

    <!-- ==== HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries ==== -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<body>

    <!-- Preloader Start -->
    <div id="preloader">
        <div class="preloader--inner"></div>
    </div>
    <!-- Preloader End -->

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Header Section Start -->
        @include('citizenask.layout.header')
        <!-- Header Section End -->

        <!-- Banner Section Start -->
        @yield('content')
        <!-- FAQ and Download Section End -->

        <!-- Footer Section Start -->
        @include('citizenask.layout.footer')
        <!-- Footer Section End -->
    </div>
    <!-- Wrapper End -->

    <!-- Back To Top Button Start -->
    <div id="backToTop">
        <a href="#" class="btn"><i class="fa fa-caret-up"></i></a>
    </div>
    <!-- Back To Top Button End -->
    {{-- chatter package js --}}
        @yield('js')

    <!-- ==== Plugins Bundle ==== -->
    <script src="/citizenask/js/plugins.min.js"></script>

    <!-- ==== Color Switcher Plugin ==== -->

    <!-- ==== Main Script ==== -->
    <script src="/citizenask/js/main.js"></script>

    <!-- ==== Custom Script ==== -->
    <script src="/citizenask/js/custom.js"></script>
    <script src="../../admin/bower_components/select2/dist/js/select2.full.min.js"></script>
    <!-- ==== Vue js ==== -->
    <script>
        var popupSize = {
            width: 780,
            height: 550
        };

        $(document).on('click', '.social-buttons > a', function (e) {

            var
                verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
                horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

            var popup = window.open($(this).prop('href'), 'social',
                'width=' + popupSize.width + ',height=' + popupSize.height +
                ',left=' + verticalPos + ',top=' + horisontalPos +
                ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

            if (popup) {
                popup.focus();
                e.preventDefault();
            }

        });
    </script>

{{-- this is for any additional script from different pages --}}
@yield('script')


</body>

<!-- Mirrored from themelooks.us/demo/socifly/html/home-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 May 2018 06:55:16 GMT -->

</html>