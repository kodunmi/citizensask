<header class="header--section style--1">
    <!-- Header Topbar Start -->
    <div class="header--topbar bg-black">
        <div class="container"> 
            <!-- Header Topbar Links Start -->
            @auth
            <ul class="header--topbar-links nav ff--primary float--left">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <span>
                            @if (empty(auth()->user()->profile_image))
                                <img src="https://www.gravatar.com/avatar/{{hash('md5',auth()->user()->email)}}?s=80&d=wavatar" class="profile-circle"/>
                            @else
                        <img src="/users/images/small/{{auth()->user()->profile_image}}" alt="" class="profile-circle"/>
                            @endif
                            </span>
                    </a>
                    <ul class="dropdown-menu">
                    <li class="active"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                        </li>
                    </ul>
                </li>
            </ul>  
            @endauth

            <!-- Header Topbar Links End -->

            <!-- Header Topbar Social Start -->
            <ul class="header--topbar-social nav float--left hidden-xs">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
            </ul>
            <!-- Header Topbar Social End -->

            <!-- Header Topbar Links Start -->
            <ul class="header--topbar-links nav ff--primary float--right">
                @if (auth()->check())
                <li class="dropdown">
                    <a href="{{route('dashboard')}}" class="btn-link dropdown-toggle" data-toggle="dropdown">
                        <i class="fa mr--8 fa-user-o"></i>
                        <span>{{auth()->user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                            <li class="active"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                                </li>
                            </ul>
                </li>   
                @else
                <li class="dropdown {{Route::currentRouteName() == 'login' ? 'active':''||Route::currentRouteName() == 'register' ? 'active':''}}">
                        <a href="#" class="dropdown-toggle btn-link" data-toggle="dropdown">
                            <i class="fa mr--8 fa-user-o"></i>
                            <span>My Account</span>  
                        </a>
                        <ul class="dropdown-menu">
                            <li class="{{Route::currentRouteName() == 'login' ? 'active':''}}"><a href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            <li class="{{Route::currentRouteName() == 'register' ? 'active':''}}"><a href="{{ route('register') }}">{{ __('Register') }}</a></li>    
                        </ul>
                    </li>
                @endif
                
            </ul>
            <!-- Header Topbar Links End -->
        </div>
    </div>
    <!-- Header Topbar End -->

    <!-- Header Navbar Start -->
    <div class="header--navbar navbar bg-black " data-trigger="sticky">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle style--1 collapsed" data-toggle="collapse" data-target="#headerNav">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Header Navbar Logo Start -->
                <div class="header--navbar-logo navbar-brand">
                    <a href="index.html">
                        <img src="/citizenask/img/logo-white.png" class="normal" alt="">
                        <img src="/citizenask/img/logo-black.png" class="sticky" alt="">
                    </a>
                </div>
                <!-- Header Navbar Logo End -->
            </div>

            <div id="headerNav" class="navbar-collapse collapse float--right">
                <!-- Header Nav Links Start -->
                <ul class="header--nav-links style--1 nav ff--primary">
                    <li class="dropdown {{Route::currentRouteName() == 'citizenask' ? 'active':''}}">
                        <a href="{{route('citizenask')}}"><span>Home</span></a>
                    </li>
                    <li class="dropdown {{Route::currentRouteName() == 'templates'||Route::currentRouteName() == 'howToAsk'||Route::currentRouteName() == 'experience' ? 'active':''}}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span>Asking</span>
                            <i class="fa fa-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
                            <li class="{{Route::currentRouteName() == 'howToAsk' ? 'active':''}}">
                                <a href="{{route('howToAsk')}}">How To Ask</a>
                            </li>

                            <li class="{{Route::currentRouteName() == 'templates' ? 'active':''}}">
                                <a href="{{route('templates')}}">Templates</a>
                            </li>
                            <li class="{{Route::currentRouteName() == 'experience' ? 'active':''}}">
                                <a href="{{route('experience')}}">Share Experience</a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{Route::currentRouteName() == 'about' ? 'active':''}}"><a href="{{route('about')}}"><span>About</span></a></li>
                    <li class="{{Route::currentRouteName() == 'contact' ? 'active':''}}"><a href="{{route('contact')}}"><span>Contact</span></a></li>
                </ul>
                <!-- Header Nav Links End -->
            </div>
        </div>
    </div>
    <!-- Header Navbar End -->
</header>