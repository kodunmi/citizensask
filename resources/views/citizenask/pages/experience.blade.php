@extends('citizenask.layout.master')
@section('meta-information')

@endsection
@section('title')
Citizens Ask | Experience
@endsection
@section('content')
<div class="page--header pt--60 pb--60 text-center" data-bg-img="/citizenask/img/page-header-img/bg.jpg"
    data-overlay="0.85">
    <div class="container">
        <div class="title">
            <h2 class="h1 text-white">Experience</h2>
        </div>

        <ul class="breadcrumb text-gray ff--primary">
            <li><a href="home-1.html" class="btn-link">Home</a></li>
            <li class="active"><span class="text-primary">Experience</span></li>
        </ul>
    </div>
</div>
<!-- Page Header End -->
<!-- Page Wrapper Start -->
<section class="page--wrapper pt--80 pb--20">
    <div class="container">
        <div class="row">
            <!-- Main Sidebar Start -->
            <div class="main--sidebar col-md-4 pb--60" data-trigger="stickyScroll">
                <!-- Widget Start -->
                <div class="widget">
                    <h2 class="h4 fw--700 widget--title">Share Experience</h2>
                    @include('admin.layout.error')
                    @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                    @endif
                    <!-- Buddy Finder Widget Start -->
                    <div class="buddy-finder--widget">
                        <form action="/experience" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-xs-6 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Choose Category Of Template
                                                Used</span>

                                            <select name="category_id" class="form-control form-sm" id="category"
                                                required>
                                                @foreach ($cats as $cat)
                                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Choose The Template
                                                Used</span>

                                            <select name="template_id" class="form-control form-sm" id="template"
                                                required>

                                            </select>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Experience Title</span>
                                            <input name="title" type="text" class="form-control form-sm" required>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Experience Body</span>

                                            <textarea name="body" class="form-control form-sm" required>

                                                </textarea>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    @auth
                                    <button type="submit" class="btn btn-primary">Share Experience</button>
                                    @endauth
                                    @guest
                                    <a href="/login"><button type="button" class="btn btn-primary">Login To Share
                                            Experience</button></a>
                                    @endguest
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Buddy Finder Widget End -->
                </div>
                <!-- Widget End -->
            </div>
            <!-- Main Sidebar End -->
            <!-- Main Content Start -->
            <div class="main--content col-md-8 pb--60" data-trigger="stickyScroll">
                <div class="main--content-inner">
                    <!-- Filter Nav Start -->
                    <div class="filter--nav pb--30 clearfix">
                        <div class="filter--link float--left">
                            <h2 class="h4">All Experience : 30,000</h2>
                        </div>
                    </div>
                    <!-- Filter Nav End -->

                    <!-- Member Items Start -->
                    <div class="member--items">
                        <div class="row gutter--15 AdjustRow">
                            @foreach ($exps as $exp)
                            <div class="col-sm-6 col-xs-12 pb--30">
                                <!-- Post Item Start -->
                                <div class="post--item" data-scroll-reveal="bottom">
                                    <!-- Post Info Start -->
                                    <div class="post--info">
                                        <!-- Post Meta Start -->
                                        <div class="post--meta">
                                            <ul class="nav">
                                                <li>
                                                    <a href="/user/{{str_replace(' ','',$exp->user->name)}}/{{$exp->user->id}}">
                                                        @auth
                                                            
                                                        @endauth
                                                        @if (empty($exp->user->profile_image))
                                                            <img src="https://www.gravatar.com/avatar/{{hash('md5',$exp->user->email)}}?s=80&d=wavatar" class="profile-circle"/>
                                                        @else
                                                            <img src="/users/images/small/{{$exp->user->profile_image}}" alt="" class="profile-circle"/>
                                                        @endif
                                                        <span>{{$exp->user->name}}</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="mr--8 fa fa-calendar-o"></i>
                                                        <span>{{$exp->created_at->diffForHumans()}}</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="mr--8 fa fa-comments-o"></i>
                                                        <span>{{count($exp->comments)}} Comment</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- Post Meta End -->

                                        <!-- Post Title Start -->
                                        <div class="post--title mt--10">
                                            <h3 class="h6">

                                                <a href="/experience/show/{{$exp->id}}"
                                                    class="btn-link">{{Str::words($exp->body, 21)}}</a>


                                            </h3>
                                        </div>
                                        <!-- Post Title End -->

                                        <!-- Post Meta Start -->
                                        <div class="post--meta">
                                            <ul class="nav">
                                                <li>
                                                    <i class="mr--8 fa fa-folder-open-o"></i>

                                                    <a
                                                        href="/templates/category/{{$exp->template->category->id}}"><span>{{$exp->template->category->name}}</span></a>
                                                </li>
                                                <li>
                                                    <i class="mr--8 fa fa-tags"></i>

                                                    <a
                                                        href="/template/show/{{$exp->template->id}}"><span>{{$exp->template->title}}</span></a>

                                                </li>
                                            </ul>
                                        </div>
                                        <!-- Post Meta End -->

                                        <!-- Post Action Start -->

                                        <div class="post--action text-darkest mt--8">
                                            <a href="/experience/show/{{$exp->id}}" class="btn-link">Continue Reading<i
                                                    class="ml--10 text-primary fa fa-caret-right"></i></a>
                                        </div>


                                        <!-- Post Action End -->
                                    </div>
                                    <!-- Post Info End -->
                                </div>
                                <!-- Post Item End -->
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- Member Items End -->

                    <!-- Page Count Start -->
                    <div class="page--count pt--30">
                        <label class="ff--primary fs--14 fw--500 text-darker">
                            <span>Viewing</span>

                            <a href="#" class="btn-link"><i class="fa fa-caret-left"></i></a>
                            <input type="number" name="page-count" value="01" class="form-control form-sm">
                            <a href="#" class="btn-link"><i class="fa fa-caret-right"></i></a>

                            <span>of 2,500</span>
                        </label>
                    </div>
                    <!-- Page Count End -->
                </div>
            </div>
            <!-- Main Content End -->

        </div>
    </div>
</section>
<!-- Page Wrapper End -->
@endsection
@section('script')
<script type="text/javascript">
    $('#category').select2();
    $('#template').select2();


    var template = $('#template');
    $(document).ready(function () {
        $("#category").change(function () {
            var selectedCat = $(this).children("option:selected").val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: '/category/templates/' + selectedCat,
                dataType: 'json',
                success: function (data) {
                    console.log(data.tmps.length);
                    for (var i = 0; i < data.tmps.length; i++) {
                        var option = new Option(data.tmps[i].title, data.tmps[i].id);
                        template.append(option).trigger('change');
                    }
                },
                error: function () {
                    alert('failed');
                }
            });
            $('#template').empty();
        });
    });
</script>
@endsection