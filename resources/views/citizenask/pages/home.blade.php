@extends('citizenask.layout.master')
@section('meta-information')
    <meta property="og:url"           content="http://127.0.0.1:8000/citizenask" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="learn how to ask your government question" />
    <meta property="og:description"   content="citizen ask is a platform where nigerian citizen and people all over the world can learn how to ask their government questions and hold them responsible, its also a platform to download tailor made templates and letters to ask the government questions. Join the forum of people who are learning to ask WHY!!!"/>
    <meta property="og:image"         content="http://127.0.0.1:8000/citizenask/img/banner-img/home-version-1/slider-bg-01-new.fw.png" />
<!—- ShareThis BEGIN -—>
<script async src="https://platform-api.sharethis.com/js/sharethis.js#property=5d25e693aca1ef0012990ac7&product='sticky-share-buttons'"></script>
<!—- ShareThis END -—>
@endsection
@section('title')
    Citizens Ask
@endsection
@section('content')
<section class="banner--section">
    <!-- Banner Slider Start -->
    <div class="banner--slider owl-carousel" data-owl-dots="true" data-owl-dots-style="2">
        <!-- Banner Item Start -->
        <div class="banner--item" data-bg-img="/citizenask/img/banner-img/home-version-1/slider-bg-01.jpg" data-overlay="0.45">
            <div class="vc--parent">
                <div class="vc--child">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <!-- Banner Content Start -->
                                <div class="banner--content pt--70 pb--80 text-center">
                                    <div class="title">
                                        <h1 class="h1 text-lightgray">Welcome To Our Community</h1>
                                    </div>

                                    <div class="sub-title">
                                        <h2 class="h2 text-lightgray">Connect, Engage &amp; Share</h2>
                                    </div>

                                    <div class="desc text-gray fs--16">
                                        <p style="color:#fff;">Citizen Ask is a capacity building initiative of Youngstars Foundation that intends to build the capacity of Citizens (Individuals not just Civil Society Organizations) on how to ask Government Officials and Agencies questions so as to promote Accountability and Good Governance.</p>
                                    </div>

                                    <div class="action text-uppercase">
                                        <a href="/about" class="btn btn-white">Learn More</a>
                                        <a href="/templates" class="btn btn-primary">See Templates</a>
                                    </div>
                                </div>
                                <!-- Banner Content End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Banner Item End -->

        <!-- Banner Item Start -->
        <div class="banner--item" data-bg-img="/citizenask/img/banner-img/home-version-1/slider-bg-02.jpg" data-overlay="0.45">
            <div class="vc--parent">
                <div class="vc--child">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <!-- Banner Content Start -->
                                <div class="banner--content pt--70 pb--80 text-center">
                                    <div class="title">
                                        <h1 class="h1 text-lightgray">Welcome To Our Community</h1>
                                    </div>

                                    <div class="sub-title">
                                        <h2 class="h2 text-lightgray">Connect, Engage &amp; Share</h2>
                                    </div>

                                    <div class="desc text-gray fs--16">
                                        <p style="color:#fff;">Citizen Ask is a capacity building initiative of Youngstars Foundation that intends to build the capacity of Citizens (Individuals not just Civil Society Organizations) on how to ask Government Officials and Agencies questions so as to promote Accountability and Good Governance. </p>
                                    </div>

                                    <div class="action text-uppercase">
                                        <a href="/login" class="btn btn-white">Login</a>
                                        <a href="/register" class="btn btn-primary">Register</a>
                                    </div>
                                </div>
                                <!-- Banner Content End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Banner Item End -->

        <!-- Banner Item Start -->
        <div class="banner--item" data-bg-img="/citizenask/img/banner-img/home-version-1/slider-bg-03.jpg" data-overlay="0.45">
            <div class="vc--parent">
                <div class="vc--child">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <!-- Banner Content Start -->
                                <div class="banner--content pt--70 pb--80 text-center">
                                    <div class="title">
                                        <h1 class="h1 text-lightgray">Welcome To Our Community</h1>
                                    </div>

                                    <div class="sub-title">
                                        <h2 class="h2 text-lightgray">Connect, Engage &amp; Share</h2>
                                    </div>

                                    <div class="desc text-gray fs--16">
                                        <p style="color:#fff;">Citizen Ask is a capacity building initiative of Youngstars Foundation that intends to build the capacity of Citizens (Individuals not just Civil Society Organizations) on how to ask Government Officials and Agencies questions so as to promote Accountability and Good Governance. </p>
                                    </div>

                                    <div class="action text-uppercase">
                                        <a href="/contact" class="btn btn-white">Contact Us</a>
                                        <a href="/how-to-ask" class="btn btn-primary">How To Ask</a>
                                    </div>
                                </div>
                                <!-- Banner Content End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Banner Item End -->
    </div>
    <!-- Banner Slider End -->
</section>
<!-- Banner Section End -->

<!-- Features Section Start -->
<section class="section bg-lighter pt--80 pb--40">
    <div class="container">
        <div class="row AdjustRow">
            <div class="col-md-3 col-xs-6 col-xxs-12 pb--40">
                <!-- Feature Item Start -->
                <div class="feature--item bg-default text-center">
                    <div class="title">
                    <h2 class="h1 ff--default text-primary"><span data-trigger="counterup">{{$templates->count()}}</span></h2>
                    </div>

                    <div class="sub-title">
                        <h3 class="h2 fs--16">Asks Templates Available</h3>
                    </div>

                    <div class="desc">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
                <!-- Feature Item End -->
            </div>

            <div class="col-md-3 col-xs-6 col-xxs-12 pb--40">
                <!-- Feature Item Start -->
                <div class="feature--item bg-default text-center">
                    <div class="title">
                    <h2 class="h1 ff--default text-primary"><span data-trigger="counterup">{{$numbersOfDownloads}}</span></h2>
                    </div>

                    <div class="sub-title">
                        <h3 class="h2 fs--16">Asks Templates Downloaded</h3>
                    </div>

                    <div class="desc">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
                <!-- Feature Item End -->
            </div>

            <div class="col-md-3 col-xs-6 col-xxs-12 pb--40">
                <!-- Feature Item Start -->
                <div class="feature--item bg-default text-center">
                    <div class="title">
                    <h2 class="h1 ff--default text-primary"><span data-trigger="counterup">{{$numbersOfUsers}}</span></h2>
                    </div>

                    <div class="sub-title">
                        <h3 class="h2 fs--16">Registered Members</h3>
                    </div>

                    <div class="desc">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
                <!-- Feature Item End -->
            </div>

            <div class="col-md-3 col-xs-6 col-xxs-12 pb--40">
                <!-- Feature Item Start -->
                <div class="feature--item bg-default text-center">
                    <div class="title">
                        <h2 class="h1 ff--default text-primary"><span data-trigger="counterup">{{$exps->count()}}</span></h2>
                    </div>

                    <div class="sub-title">
                        <h3 class="h2 fs--16">Follow ups & Experience shared</h3>
                    </div>

                    <div class="desc">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
                <!-- Feature Item End -->
            </div>
        </div>
    </div>
</section>
<!-- Features Section End -->


<!-- How It Works Section Start -->
<section class="section pt--70 pb--40">
    <div class="container">
        <!-- Section Title Start -->
        <div class="section--title pb--50 text-center">
            <div class="title">
                <h2 class="h2">How It Works?</h2>
            </div>

            <!-- <div class="sub-title">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has industry's.</p>
            </div> -->
        </div>
        <!-- Section Title End -->

        <div class="row">
            <div class="col-md-7 pb--40" style="padding-top:50px;">
                <div class="row gutter--15 AdjustRow" data-scroll-reveal="group">
                    <div class="col-xs-4 pb--15">
                        <img src="/citizenask/img/how-it-works-img/01.jpg" alt="">
                    </div>

                    <div class="col-xs-4 pb--15">
                        <img src="/citizenask/img/how-it-works-img/02.jpg" alt="">
                    </div>

                    <div class="col-xs-4 pb--15">
                        <img src="/citizenask/img/how-it-works-img/03.jpg" alt="">
                    </div>

                    <div class="col-xs-12">
                        <img src="/citizenask/img/how-it-works-img/04.jpg" alt="">
                    </div>
                </div>
            </div>

            <div class="col-md-5 pb--40">
                <!-- Info Items Start -->
                <div class="info--items" data-scroll-reveal="group">
                    <!-- Info Item Start -->
                    <div class="info--item clearfix">
                        <a href="{{route('howToAsk')}}">
                            <div class="icon">
                                <img src="/citizenask/img/how-it-works-img/icon-03.png" alt="">
                            </div>
                        </a>
                        

                        <div class="info">
                            <a href="{{route('howToAsk')}}">
                                <div class="title">
                                    <h3 class="h4 fw--700">Learn How  To Ask Government Questions & Engage Government</h3>
                                </div>
                            </a>
                            

                            <div class="desc">
                                <p>Find great deals on your favourite online stores &amp; click via our link &amp; shop as usual.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Info Item End -->

                    <!-- Info Item Start -->
                    <div class="info--item clearfix">
                        <a href="{{route('templates')}}">
                            <div class="icon">
                                <img src="/citizenask/img/how-it-works-img/icon-01.png" alt="">
                            </div>
                        </a> 
                            <div class="info">
                                <a href="{{route('templates')}}">
                                    <div class="title">
                                        <h3 class="h4 fw--700">Get Tailored Templates on How to Engage Government</h3>
                                    </div>
                                </a>
                                <div class="desc">
                                    <p>Sit back and relax. We will track your purchase and add cash to your account. Yes, that simple!</p>
                                </div>
                            </div>
                         
                    </div>
                    <!-- Info Item End -->

                    <!-- Info Item Start -->
                    <div class="info--item clearfix">
                        <a href="{{route('experience')}}">
                            <div class="icon">
                                <img src="/citizenask/img/how-it-works-img/icon-02.png" alt="">
                            </div>
                        </a>
                       

                        <div class="info">
                            <a href="{{route('experience')}}">
                                <div class="title">
                                    <h3 class="h4 fw--700">Submit Your Experience and Updates on Questions Asked</h3>
                                </div>
                            </a>
                            

                            <div class="desc">
                                <p>Refer your friends to get earn <b><u>Lifetime Rewards</u></b> on every activities performed by "<b><u>Friends &amp; Friends of Friends</u></b>" on <b>Payoffads</b>.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Info Item End -->

                    <!-- Info Item Start -->
                    <div class="info--item clearfix">
                        <div class="icon">
                            <img src="/citizenask/img/how-it-works-img/icon-04.png" alt="">
                        </div>

                        <div class="info">
                            <div class="title">
                                <h3 class="h4 fw--700">Share on Social Media to Amplify Question Asked</h3>
                            </div>

                            <div class="desc">
                                <p>We will track your <b><u>Rewards</u></b> &amp; convert to cash then add cash to your account. Yes, that simple!</p>
                            </div>
                        </div>
                    </div>
                    <!-- Info Item End -->
                </div>
                <!-- Info Items End -->
            </div>
        </div>
    </div>
</section>
<!-- How It Works Section End -->

<!-- Features Section Start -->
<section class="section bg-lighter pt--80 pb--40">
    
    <div class="container">
        <!-- Section Title Start -->
        <div class="section--title pb--50 text-center">
            <div class="title">
                <h3>Signup to learn how to start ENGAGING Government directly as an individual and hold Government ACCOUNTABLE. E.g The waste bin in Oshodi has not yet been emptied in the last 2 months, who would want to Ask a question? </h3>
            </div>
            <div class="sub-title">
                <a href="#" class="btn btn-primary">Sign up for Free?</a>    
            </div>
            
        </div>
        <!-- Section Title End -->

        
    </div>
</section>
<!-- Features Section End -->

<!-- Most Popular Groups Section Start -->
<section class="section  pt--70 pb--70">
    <div class="container">
        <!-- Box Nav Start -->
        <div class="box--nav clearfix">
            <h2 class="h2 fw--600 float--left">SOME OF AVAILABLE TEMPLATE</h2>

            <ul class="nav ff--primary float--right">
                <li class="active"><a href="#boxItemsTab01" class="btn btn-default" data-toggle="tab">Newest</a></li>
                
            </ul>
        </div>
        <!-- Box Nav End -->

        <!-- Tab Content Start -->
        <div class="tab-content">
            <!-- Tab Pane Start -->
            <div class="tab-pane fade in active" id="boxItemsTab01">
                <!-- Box Items Start -->
                <div class="box--items owl-carousel" data-owl-items="4" data-owl-margin="30" data-owl-autoplay="false" data-owl-responsive='{"0": {"items": "1"}, "481": {"items": "2"}, "768": {"items": "3"}, "992": {"items": "4"}}'>
                   
                            @foreach ($templates as $template)
                             <!-- Box Item Start -->
                    <div class="box--item text-center">
                                    <!-- Box Item Start -->
                                    <div class="img-wrapper">
                                        <img src="/admin/images/templates/medium/{{$template->image}}" alt="Atul Prajapati">
                                        <h2>
                                            <a href="/template/show/{{$template->id}}">
                                                <button type="button" class="btn btn-white">
                                                    View This Template
                                                </button>
                                            </a>
                                            @auth
                                            <a href="/template/download/{{$template->id}}" >
                                                <button type="button" class="btn btn-white">
                                                    Download Template
                                                </button>
                                            </a>
                                            @endauth
                                            @guest
                                            <a href="{{route('login')}}" data-toggle="modal" data-overlay="0.1">
                                                <button type="button" class="btn btn-white">
                                                    Login To Download
                                                </button>
                                            </a>
                                            @endguest

                                        </h2>
                                        <ul>
                                            <li class="social-buttons">
                                                <a href="https://www.facebook.com/sharer/sharer.php?u=http://127.0.0.1:8000/template/show/{{$template->id}}" target="_blank">
                                                    <i class="fa fa-facebook-square"></i>
                                                </a>
                                            </li>
                                            <li class="social-buttons">
                                                <a href="https://twitter.com/share?text={{$template->title}}&url=http://127.0.0.1:8000/template/show/{{$template->id}}&hashtags=citizenask,youngstars,weNeedToAsk&via=youngstarsfoundation">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- Box Item End -->
                                    <div>
                                        <p class="description">{!!str_limit($template->title, 100)!!}</p>
                                    </div>
                                </div>
                    <!-- Box Item End -->
                    @if ($loop->iteration == 4)
                        @break
                    @endif
                    @endforeach
                    
                </div>
                <!-- Box Items End -->

                <!-- Box Controls Start -->
                <div class="box--controls text-center">
                    <a href="#" class="btn fs--16 btn-default" data-action="prev">
                        <i class="fa fa-caret-left"></i>
                    </a>

                <a href="{{route('templates')}}" class="btn ff--primary fw--500 btn-default">View All Templates</a>
                    <a href="#" class="btn fs--16 btn-default" data-action="next">
                        <i class="fa fa-caret-right"></i>
                    </a>
                </div>
                <!-- Box Controls End -->
            </div>
            <!-- Tab Pane End -->
        </div>
        <!-- Tab Content End -->
    </div>
</section>
<!-- Most Popular Groups Section End -->

<!-- Members and Testimonial Section Start -->
<section class="section bg-lighter pt--70 pb--20" style="display:block;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 pb--60">
                <!-- Section Title Start -->
                <div class="section--title pb--20">
                    <div class="title">
                        <h2 class="h2">Some SharedExperiences</h2>
                    </div>
                </div>
                <!-- Section Title End -->

                <!-- Member Slider Start -->
                <div class="member--slider owl-carousel" data-owl-items="4" data-owl-autoplay="false" data-bg-img="/citizenask/img/members-img/pattern-bg.png" data-owl-nav="true" data-owl-center="true" data-owl-responsive='{"0": {"items": "1"}, "486": {"items": "3"}, "992": {"items": "4"}}'>
                    @foreach ($exps as $exp)
                    <!-- Member Item Start -->
                    <div class="member--item online">
                        <div class="vc--parent">
                            <div class="vc--child">    
                                <!-- Post Info Start -->
                                <div class="post--info">
                                    <!-- Post Meta Start -->
                                    <div class="post--meta">
                                        <ul class="nav">
                                            <li>
                                                <a href="/user/{{str_replace(' ','',$exp->user->name)}}/{{$exp->user->id}}">
                                                    @if (empty($exp->user->profile_image))
                                                            <img src="https://www.gravatar.com/avatar/{{hash('md5',$exp->user->email)}}?s=80&d=wavatar" class="profile-circle"/>
                                                        @else
                                                            <img src="/users/images/small/{{$exp->user->profile_image}}" alt="" class="profile-circle"/>
                                                        @endif
                                                    <span>{{$exp->user->name}}</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="mr--8 fa fa-calendar-o"></i>
                                                    <span>{{$exp->created_at->diffForHumans()}}</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="mr--8 fa fa-comments-o"></i>
                                                    <span>{{count($exp->comments)}} Comment</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- Post Meta End -->

                                    <!-- Post Title Start -->
                                    <div class="post--title mt--10">
                                        <h3 class="h6">
                                                
                                        <a href="/experience/show/{{$exp->id}}" class="btn-link">{{Str::words($exp->body, 10)}}</a>
                                                
                                            
                                        </h3>
                                    </div>
                                    <!-- Post Title End -->

                                    <!-- Post Meta Start -->
                                    <div class="post--meta">
                                        <ul class="nav">
                                            <li>
                                                <i class="mr--8 fa fa-folder-open-o"></i>

                                                <a href="/templates/category/{{$exp->template->category->id}}"><span>{{$exp->template->category->name}}</span></a>
                                            </li>
                                            <li>
                                                <i class="mr--8 fa fa-tags"></i>

                                            <a href="/template/show/{{$exp->template->id}}"><span>{{$exp->template->title}}</span></a>
                                                
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- Post Meta End -->

                                    <!-- Post Action Start -->
                                    
                                        <div class="post--action text-darkest mt--8">
                                        <a href="/experience/show/{{$exp->id}}" class="btn-link">Continue Reading<i class="ml--10 text-primary fa fa-caret-right"></i></a>
                                    </div>
                                    
                                    
                                    <!-- Post Action End -->
                                </div>
                                <!-- Post Info End -->            
                            </div>
                        </div>
                    </div>
                    <!-- Member Item End -->
                    @if ($loop->iteration == 4)
                        @break
                        @endif
                    @endforeach
                </div>
                <!-- Member Slider End -->
            </div>

            {{-- <div class="col-md-5 pb--60">
                <!-- Section Title Start -->
                <div class="section--title pb--20">
                    <div class="title">
                        <h2 class="h2">Show Some Sweet Love</h2>
                    </div>
                </div>
                <!-- Section Title End -->

                <!-- Testimonial Items Start -->
                <div class="testimonial--items owl-carousel" data-owl-dots="true" data-owl-margin="10" data-owl-autoplay="false" data-owl-animate="fadeOut">
                    <!-- Testimonial Item Start -->
                    <div class="testimonial--item clearfix">
                        

                        <div class="info">
                            <blockquote class="fs--12">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ever.</p>
                            </blockquote>

                            <div class="name">
                                <h3 class="h6 fs--12">Philip K. Rice &amp; Nina B. Bandy</h3>
                            </div>

                            <div class="rating">
                                <ul class="nav">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Testimonial Item End -->

                    <!-- Testimonial Item Start -->
                    <div class="testimonial--item clearfix">
                        

                        <div class="info">
                            <blockquote class="fs--12">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ever.</p>
                            </blockquote>

                            <div class="name">
                                <h3 class="h6 fs--12">Philip K. Rice &amp; Nina B. Bandy</h3>
                            </div>

                            <div class="rating">
                                <ul class="nav">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Testimonial Item End -->

                    <!-- Testimonial Item Start -->
                    <div class="testimonial--item clearfix">
                        <div class="img mr--20 float--left">
                            <img src="/citizenask/img/testimonial-img/03.jpg" alt="">
                        </div>

                        <div class="info">
                            <blockquote class="fs--12">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ever.</p>
                            </blockquote>

                            <div class="name">
                                <h3 class="h6 fs--12">Philip K. Rice &amp; Nina B. Bandy</h3>
                            </div>

                            <div class="rating">
                                <ul class="nav">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Testimonial Item End -->
                </div>
                <!-- Testimonial Items End -->
            </div> --}}
        </div>
    </div>
</section>
<!-- Members and Testimonial Section End -->

<!-- Why Choose Us Section Start -->
<section class="section pt--80 pb--20" style="display:none;">
    <div class="container">
        <div class="row row--md-vc">
            <div class="col-md-6 pb--50">
                <!-- Text Block Start -->
                <div class="text--block pb--10">
                    <div class="title">
                        <h2 class="h2 fw--600">Why Choose Us?</h2>
                    </div>

                    <div class="content fs--14">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. make a type specimen book.</p>
                    </div>
                </div>
                <!-- Text Block End -->

                <div class="row AdjustRow">
                    <div class="col-xs-6 col-xxs-12 pb--10">
                        <!-- Feature Block Start -->
                        <div class="feature--block mb--6 clearfix">
                            <div class="icon fs--18 text-primary mr--20 float--left">
                                <i class="fa fa-comments-o"></i>
                            </div>

                            <div class="info ov--h">
                                <div class="title">
                                    <h2 class="h6 fw--700">Individual Live Chat</h2>
                                </div>

                                <div class="desc mt--8">
                                    <p>Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Feature Block End -->
                    </div>
                    
                    <div class="col-xs-6 col-xxs-12 pb--10">
                        <!-- Feature Block Start -->
                        <div class="feature--block mb--6 clearfix">
                            <div class="icon fs--18 text-primary mr--20 float--left">
                                <i class="fa fa-wrench"></i>
                            </div>

                            <div class="info ov--h">
                                <div class="title">
                                    <h2 class="h6 fw--700">User Friendly Settings</h2>
                                </div>

                                <div class="desc mt--8">
                                    <p>Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Feature Block End -->
                    </div>
                    
                    <div class="col-xs-6 col-xxs-12 pb--10">
                        <!-- Feature Block Start -->
                        <div class="feature--block mb--6 clearfix">
                            <div class="icon fs--18 text-primary mr--20 float--left">
                                <i class="fa fa-group"></i>
                            </div>

                            <div class="info ov--h">
                                <div class="title">
                                    <h2 class="h6 fw--700">Discover Amazing People</h2>
                                </div>

                                <div class="desc mt--8">
                                    <p>Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Feature Block End -->
                    </div>

                    <div class="col-xs-6 col-xxs-12 pb--10">
                        <!-- Feature Block Start -->
                        <div class="feature--block mb--6 clearfix">
                            <div class="icon fs--18 text-primary mr--20 float--left">
                                <i class="fa fa-clock-o"></i>
                            </div>

                            <div class="info ov--h">
                                <div class="title">
                                    <h2 class="h6 fw--700">Times Fly With Us</h2>
                                </div>

                                <div class="desc mt--8">
                                    <p>Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Feature Block End -->
                    </div>
                </div>
            </div>

            <div class="col-md-6 pb--60">
                <!-- Video Popup Start -->
                <div class="video--popup style--1" data-overlay="0.3">
                    <img src="/citizenask/img/why-choose-us-img/video-poster-1.jpg" alt="">

                    <a href="https://www.youtube.com/watch?v=YE7VzlLtp-4" class="btn-link" data-trigger="video_popup">
                        <span><i class="fa fa-play"></i></span>
                    </a>
                </div>
                <!-- Video Popup End -->
            </div>
        </div>
    </div>
</section>
<!-- Why Choose Us Section End -->

<!-- FAQ and Download Section Start -->
<section class="section bg-lighter pt--70 pb--20" style="display:none;">
    <div class="container">
        <div class="row">
            <div class="col-md-5 pb--60">
                <!-- FAQ Items Start -->
                <div class="faq--items" id="faqItems" data-scroll-reveal="group">
                    <div class="title pb--20">
                        <h2 class="h2 fw--600">Frequently Asked Question</h2>
                    </div>

                    <!-- FAQ Item Start -->
                    <div class="faq--item style--1 panel">
                        <div class="title">
                            <h3 class="h6 fw--700 text-darker">
                                <a href="#faqItem01" data-parent="#faqItems" data-toggle="collapse" class="collapsed">
                                    <span>How can I register?</span>
                                </a>
                            </h3>
                        </div>

                        <div id="faqItem01" class="content collapse">
                            <div class="content--inner">
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words even slightly believable.</p>
                            </div>
                        </div>
                    </div>
                    <!-- FAQ Item End -->

                    <!-- FAQ Item Start -->
                    <div class="faq--item style--1 panel">
                        <div class="title">
                            <h3 class="h6 fw--700 text-darker">
                                <a href="#faqItem02" data-parent="#faqItems" data-toggle="collapse" class="collapsed">
                                    <span>What are the benefits of premium plans?</span>
                                </a>
                            </h3>
                        </div>

                        <div id="faqItem02" class="content collapse">
                            <div class="content--inner">
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words even slightly believable.</p>
                            </div>
                        </div>
                    </div>
                    <!-- FAQ Item End -->

                    <!-- FAQ Item Start -->
                    <div class="faq--item style--1 panel">
                        <div class="title">
                            <h3 class="h6 fw--700 text-darker">
                                <a href="#faqItem03" data-parent="#faqItems" data-toggle="collapse" class="collapsed">
                                    <span>How can I renew my plan?</span>
                                </a>
                            </h3>
                        </div>

                        <div id="faqItem03" class="content collapse">
                            <div class="content--inner">
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words even slightly believable.</p>
                            </div>
                        </div>
                    </div>
                    <!-- FAQ Item End -->
                </div>
                <!-- FAQ Items End -->
            </div>

            <div class="col-md-7 pb--60">
                <!-- Download Block Start -->
                <div class="download--block" data-scroll-reveal="group">
                    <div class="img">
                        <img src="/citizenask/img/download-img/mobile.png" alt="">
                    </div>

                    <div class="info">
                        <div class="title">
                            <h2 class="h2 fw--600">Download Application &amp; Get More Features</h2>
                        </div>

                        <div class="content fs--12">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industtry's statndard dummy specimen book. Lorem Ipsum which looks reasonable. The generated Lorem Ipsum words etc.</p>
                        </div>

                        <div class="action text-uppercase">
                            <a href="#" class="btn btn-sm btn-google"><i class="fa mr--8 fa-play"></i>Play Store</a>
                            <a href="#" class="btn btn-sm btn-apple"><i class="fa mr--8 fa-apple"></i>App Store</a>
                        </div>
                    </div>
                </div>
                <!-- Download Block End -->
            </div>
        </div>
    </div>
</section>
@endsection