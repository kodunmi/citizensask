@extends('citizenask.layout.master')
@section('title')
Citizens Ask | Dashboard
@endsection
@section('meta-information')
    <meta name="csrf-token" content="{{ csrf_token() }}" />    
@endsection
@section('content')
<div class="cover--header pt--80 text-center" data-bg-img="/citizenask/img/cover-header-img/bg-01.jpg"
    data-overlay="0.6" data-overlay-color="white">
    <div class="container">
        <div class="cover--avatar online" data-overlay="0.3" data-overlay-color="primary">
            @if (empty($user->profile_image))
                <img src="https://www.gravatar.com/avatar/{{hash('md5',$user->email)}}?s=80&d=wavatar" class="profile-circle"/>
            @else
                <img src="/users/images/large/{{$user->profile_image}}" alt="" class="profile-circle"/>
            @endif
        </div>
        <div class="cover--user-name">
            <h2 class="h3 fw--600">{{$user->name}}</h2>
        </div>
        <div class="cover--user-activity">
            <p><i class="fa mr--8 fa-clock-o"></i>Membber Since {{$user->created_at->diffForHumans()}}</p>
        </div>
        <div class="cover--user-desc fw--400 fs--18 fstyle--i text-darkest">
            <p>Hello everyone ! There are many variations of passages of Lorem Ipsum available, but the majority have
                suffered alteration in some form, by injected humour.</p>
        </div>
    </div>
</div>
<section class="page--wrapper pt--80 pb--20">
    <div class="container">
        <div class="row">
            <!-- Main Content Start -->
            <div class="main--content col-md-12 pb--60" data-trigger="stickyScroll">
                <div class="main--content-inner drop--shadow">
                    <!-- Content Nav Start -->
                    <div class="content--nav pb--30">
                        <ul class="nav ff--primary fs--14 fw--500 bg-lighter">
                            <li class="active"><a>Profile</a></li>
                        </ul>
                    </div>
                    <!-- Content Nav End -->

                    <!-- Profile Details Start -->
                    <div class="profile--details fs--14">
                        <!-- Profile Item Start -->
                        <div class="profile--item">
                            <div class="profile--heading">
                                <h3 class="h4 fw--700">
                                    <span class="mr--4">About Me</span>
                                    <i class="ml--10 text-primary fa fa-caret-right"></i>
                                </h3>
                            </div>
                            <div class="profile--info">
                                <table class="table">
                                    <tr>
                                        <th class="fw--700 text-darkest">Full Name</th>
                                        <td><a href="#" class="btn-link">{{$user->name}}</a></td>
                                    </tr>
                                    <tr>
                                        <th class="fw--700 text-darkest">Age Range</th>
                                        <td>{{$user->age_range}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- Profile Item End -->

                        <!-- Profile Item Start -->
                        <div class="profile--item">
                            <div class="profile--heading">
                                <h3 class="h4 fw--700">
                                    <span class="mr--4">Biography</span>
                                    <i class="ml--10 text-primary fa fa-caret-right"></i>
                                </h3>
                            </div>
                            <div class="profile--info">
                                <p>{{$user->about_me}}</p>
                            </div>
                        </div>
                        <!-- Profile Item End -->

                        <!-- Profile Item Start -->
                        
                        <!-- Profile Item End -->

                        <!-- Profile Item Start -->
                        <div class="profile--item">
                            <div class="profile--heading">
                                <h3 class="h4 fw--700">
                                    <span class="mr--4">Contact</span>
                                    <i class="ml--10 text-primary fa fa-caret-right"></i>
                                </h3>
                            </div>

                            <div class="profile--info">
                                <table class="table">
                                    <tr>
                                        <th class="fw--700 text-darkest">Phone</th>
                                        <td>{{$user->phone}}</td>
                                    </tr>
                                    <tr>
                                        <th class="fw--700 text-darkest">E-mail</th>
                                        <td>{{$user->email}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="fw--700 text-darkest">Country</th>
                                        <td>{{$user->country}}</td>
                                    </tr>
                                    <tr>
                                        <th class="fw--700 text-darkest">State</th>
                                        <td>{{$user->state}}</td>
                                    </tr>
                                    <tr>
                                        <th class="fw--700 text-darkest">Local Govn't</th>
                                        <td>{{$user->local_government}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- Profile Item End -->
                    </div>
                    <!-- Profile Details End -->
                </div>
            </div>
            <!-- Main Content End -->

            <!-- Main Sidebar Start -->
            
            <!-- Main Sidebar End -->
        </div>
    </div>
    
    
    
</section>
@endsection