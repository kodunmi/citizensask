@extends('citizenask.layout.master')
@section('title')
    Citizens Ask | Template
@endsection
@section('meta-information')
    <meta property="og:url"           content="http://127.0.0.1:8000/template/show/{{$template->id}}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{$template->title}}" />
    <meta property="og:description"   content="{{$template->description}}"/>
    <meta property="og:image"         content="http://127.0.0.1:8000/admin/images/templates/large/{{$template->image}}" />
    <!—- ShareThis BEGIN -—>
<script async src="https://platform-api.sharethis.com/js/sharethis.js#property=5d25e693aca1ef0012990ac7&product='sticky-share-buttons'"></script>
<!—- ShareThis END -—>
@endsection
@section('content')
<div class="page--header pt--60 pb--60 text-center" data-bg-img="/citizenask/img/page-header-img/bg.jpg"
    data-overlay="0.85">
    <div class="container">
        <div class="title">
            <h2 class="h1 text-white">Template</h2>
        </div>

        <ul class="breadcrumb text-gray ff--primary">
            <li><a href="home-1.html" class="btn-link">Home</a></li>
            <li class="active"><span class="text-primary">Single Template</span></li>
        </ul>
    </div>
</div>
<div class="contact--section pt--80 pb--20">
    <section class="design-process-section" id="process-tab">
        <div class="container">
            <div class="row">
                <div class="col-md-6" data-trigger="stickyScroll">
                    <img src="\admin\images\templates\large\{{$template->image}}" alt="">
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <span>Template Title:</span>
                        <div class="description des"><p>{!!$template->title!!}</p></div>
                        <span>Template Description:</span>
                        <div><p>{!!$template->description!!}</p></div> 
                        <div class="exp">Experience People Got From Using This Template:</div>
                        @foreach ($template->experiences as $exp)
                        <div class="bottom">
                                <!-- Post Item Start -->
                                <div class="post--item" data-scroll-reveal="bottom">
                                    <!-- Post Info Start -->
                                    <div class="post--info">
                                        <!-- Post Meta Start -->
                                        <div class="post--meta">
                                            <ul class="nav">
                                                <li>
                                                    <a href="#">
                                                        @if (empty(auth()->user()->profile_image))
                                                            <img src="https://www.gravatar.com/avatar/{{hash('md5',auth()->user()->email)}}?s=80&d=wavatar" class="profile-circle"/>
                                                        @else
                                                            <img src="/users/images/small/{{auth()->user()->profile_image}}" alt="" class="profile-circle"/>
                                                        @endif
                                                    {{-- <script src='https://www.avatarapi.com/js.aspx?email={{$exp->user->email}}&size=128'></script> --}}
                                                        <span>{{$exp->user->name}}</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="mr--8 fa fa-calendar-o"></i>
                                                        <span>{{$exp->created_at->diffForHumans()}}</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="mr--8 fa fa-comments-o"></i>
                                                        <span>{{count($exp->comments)}} Comment</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- Post Meta End -->

                                        <!-- Post Title Start -->
                                        <div class="post--title mt--10">
                                            <h3 class="h6">
                                                    
                                            <a href="/experience/show/{{$exp->id}}" class="btn-link">{{Str::words($exp->body, 21)}}</a>
                                                    
                                                
                                            </h3>
                                        </div>
                                        <!-- Post Title End -->

                                        <!-- Post Meta Start -->
                                        <div class="post--meta">
                                            <ul class="nav">
                                                <li>
                                                    <i class="mr--8 fa fa-folder-open-o"></i>

                                                    <a href="/templates/category/{{$exp->template->category->id}}"><span>{{$exp->template->category->name}}</span></a>
                                                </li>
                                                <li>
                                                    <i class="mr--8 fa fa-tags"></i>

                                                <a href="/template/show/{{$exp->template->id}}"><span>{{$exp->template->title}}</span></a>
                                                    
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- Post Meta End -->

                                        <!-- Post Action Start -->
                                        
                                            <div class="post--action text-darkest mt--8">
                                            <a href="/experience/show/{{$exp->id}}" class="btn-link">Continue Reading<i class="ml--10 text-primary fa fa-caret-right"></i></a>
                                        </div>
                                        
                                        
                                        <!-- Post Action End -->
                                    </div>
                                    <!-- Post Info End -->
                                </div>
                                <!-- Post Item End -->
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
    </section>
</div>
@endsection