@extends('citizenask.layout.master')
@section('meta-information')

@endsection
@section('title')
Citizens Ask | Experience
@endsection
@section('content')
<div class="page--header pt--60 pb--60 text-center" data-bg-img="/citizenask/img/page-header-img/bg.jpg"
    data-overlay="0.85">
    <div class="container">
        <div class="title">
            <h2 class="h1 text-white">Experience</h2>
        </div>

        <ul class="breadcrumb text-gray ff--primary">
            <li><a href="home-1.html" class="btn-link">Home</a></li>
            <li class="active"><span class="text-primary">Experience</span></li>
        </ul>
    </div>
</div>
<!-- Page Header End -->
<!-- Page Wrapper Start -->
<section class="page--wrapper pt--80 pb--20">
    <div class="container">
        <div class="row">
            <!-- Main Sidebar Start -->
            <div class="main--sidebar col-md-4 pb--60" data-trigger="stickyScroll">
                <!-- Widget Start -->
                <div class="widget">

                    <h2 class="h4 fw--700 widget--title">Share Experience</h2>
                    @include('admin.layout.error')
                    @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                    @endif
                    <!-- Buddy Finder Widget Start -->
                    <div class="buddy-finder--widget">
                        <form action="/experience" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-xs-6 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Choose Category Of Template
                                                Used</span>

                                            <select name="category_id" class="form-control form-sm" id="category2"
                                                required>
                                                @foreach ($cats as $cat)
                                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Choose The Template
                                                Used</span>

                                            <select name="template_id" class="form-control form-sm" id="template2"
                                                required>

                                            </select>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Experience Title</span>
                                            <input name="title" type="text" class="form-control form-sm" required>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Experience Body</span>

                                            <textarea name="body" class="form-control form-sm" required>

                                                </textarea>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    @auth
                                    <button type="submit" class="btn btn-primary">Share Experience</button>
                                    @endauth
                                    @guest
                                    <a href="/login"><button type="button" class="btn btn-primary">Login To Share
                                            Experience</button></a>
                                    @endguest
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Buddy Finder Widget End -->
                </div>
                <!-- Widget End -->
            </div>
            <!-- Main Sidebar End -->
            <!-- Main Content Start -->
            <div class="main--content col-md-8 pb--60" data-trigger="stickyScroll">
                <div class="main--content-inner">
                    <!-- Post Item Start -->
                    <div class="post--item post--single pb--30">


                        <!-- Post Info Start -->
                        <div class="post--info">
                            <!-- Post Meta Start -->
                            <div class="post--meta">
                                <ul class="nav">
                                    <li>
                                        <li>
                                            <a href="/user/{{str_replace(' ','',$exp->user->name)}}/{{$exp->user->id}}">
                                                @if (empty($exp->user->profile_image))
                                                    <img src="https://www.gravatar.com/avatar/{{hash('md5',$exp->user->email)}}?s=80&d=wavatar" class="profile-circle"/>
                                                @else
                                                    <img src="/users/images/medium/{{$exp->user->profile_image}}" alt="" class="profile-circle"/>
                                                @endif    
                                                <span>{{$exp->user->name}}</span>
                                            </a>
                                        </li>
                                        <a href="#">
                                            <i class="mr--8 fa fa-calendar-o"></i>
                                            <span>{{$exp->created_at->diffForHumans()}}</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="mr--8 fa fa-comments-o"></i>
                                            <span>{{count($exp->comments)}} Comments</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- Post Meta End -->

                            <!-- Post Title Start -->
                            <div class="post--title mt--10">
                                <h3 class="h4">{{$exp->title}}</h3>
                            </div>
                            <!-- Post Title End -->

                            <!-- Post Content Start -->
                            <div class="post--content text-darker mt--10">
                                <p>{{$exp->body}}</p>
                            </div>
                            <!-- Post Content End -->

                            <!-- Post Footer Start -->
                            <div class="post--footer">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- Post Meta Start -->
                                        <div class="post--meta">
                                            <ul class="nav">
                                                <li>
                                                    <i class="mr--8 fa fa-folder-open-o"></i>

                                                    <a
                                                        href="/templates/category/{{$exp->template->category->id}}"><span>{{$exp->template->category->name}}</span></a>
                                                </li>
                                                <li>
                                                    <i class="mr--8 fa fa-tags"></i>

                                                    <a
                                                        href="/template/show/{{$exp->template->id}}"><span>{{$exp->template->title}}</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- Post Meta End -->
                                    </div>

                                    <div class="col-sm-6">
                                        <!-- Post Social Start -->
                                        <div class="post--social-share clearfix">
                                            <ul class="nav">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                            </ul>
                                        </div>
                                        <!-- Post Social End -->
                                    </div>
                                </div>
                            </div>
                            <!-- Post Footer End -->
                        </div>
                        <!-- Post Info End -->
                    </div>
                    <!-- Post Item End -->
                </div>
                <!-- Comment List Start -->
                <div class="comment--list pt--40">
                    <h4 class="h4 pb--20">{{count($exp->comments)}} Comments</h4>
                    @include('citizenask.layout._comment_replies', ['comments' => $exp->comments, 'exp_id' => $exp->id])
                </div>
                <!-- Comment List End -->

                <!-- Comment Form Start -->
                <div class="comment--form pb--30" data-form="validate">
                    <h4 class="h4 pb--15">Leave A Comment</h4>
                    <form method="post" action="{{ route('comment.add') }}">
                        @csrf
                        <div class="row gutter--15">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea name="comment_body" placeholder="Comment *" class="form-control"
                                        required></textarea>
                                </div>
                                <input type="hidden" name="exp_id" value="{{ $exp->id }}" />
                            </div>
                            @auth
                            <div class="col-sm-12 pt--10">
                                <button type="submit" class="btn btn-sm btn-primary fs--14">Post Comment</button>
                            </div>
                            @endauth
                            @guest
                            <div class="col-sm-12 pt--10">
                                <a href="/login"><button type="button" class="btn btn-sm btn-primary fs--14">Login To
                                        Post Comment</button></a>
                            </div>
                            @endguest

                        </div>
                    </form>
                </div>
                <!-- Comment Form End -->
                {{-- <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-body">
                                        <p><b>{{ $exp->title }}</b></p>
                                        <p>
                                            {{ $exp->body }}
                                        </p>
                                        <hr />
                                        <h4>Display Comments</h4>
                                        @include('citizenask.layout._comment_replies', ['comments' => $exp->comments, 'exp_id' => $exp->id])
                                        <hr />
                                        <h4>Add comment</h4>
                                        <form method="post" action="{{ route('comment.add') }}">
                                            @csrf
                                            <div class="form-group">
                                                <input type="text" name="comment_body" class="form-control" />
                                                <input type="hidden" name="post_id" value="{{ $exp->id }}" />
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-warning" value="Add Comment" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            <!-- Main Content End -->

        </div>
    </div>
</section>
<!-- Page Wrapper End -->
@endsection
@section('script')
<script type="text/javascript">
    $('#category2').select2();
    $('#template2').select2();


    var template = $('#template2');
    $(document).ready(function () {
        $("#category2").change(function () {
            var selectedCat = $(this).children("option:selected").val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: '/category/templates/' + selectedCat,
                dataType: 'json',
                success: function (data) {
                    console.log(data.tmps.length);
                    for (var i = 0; i < data.tmps.length; i++) {
                        var option = new Option(data.tmps[i].title, data.tmps[i].id);
                        template.append(option).trigger('change');
                    }
                },
                error: function () {
                    alert('failed');
                }
            });
            $('#template2').empty();
        });
    });
</script>
@endsection