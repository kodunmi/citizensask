@extends('citizenask.layout.master')
@section('meta-information')
    <meta property="og:url"           content="http://127.0.0.1:8000/citizenaskk" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="learn how to ask your government question" />
    <meta property="og:description"   content="citizen ask is a platform where nigerian citizen and people all over the world can learn how to ask their government questions and hold them responsible, its also a platform to download tailor made templates and letters to ask the government questions. Join the forum of people who are learning to ask WHY!!!"/>
    <meta property="og:image"         content="http://127.0.0.1:8000/citizenask/img/banner-img/home-version-1/slider-bg-01-new.fw.png" />
<!—- ShareThis BEGIN -—>
<script async src="https://platform-api.sharethis.com/js/sharethis.js#property=5d25e693aca1ef0012990ac7&product='sticky-share-buttons'"></script>
<!—- ShareThis END -—>
@endsection
@section('title')
    Citizens Ask | How To Ask
@endsection
@section('content')
<div class="page--header pt--60 pb--60 text-center" data-bg-img="/citizenask/img/page-header-img/bg.jpg"
    data-overlay="0.85">
    <div class="container">
        <div class="title">
            <h2 class="h1 text-white">How To Ask</h2>
        </div>

        <ul class="breadcrumb text-gray ff--primary">
            <li><a href="home-1.html" class="btn-link">Home</a></li>
            <li class="active"><span class="text-primary">How To Ask</span></li>
        </ul>
    </div>
</div>
<!-- Page Header End -->
<div class="contact--section pt--80 pb--20">
    <section class="design-process-section" id="process-tab">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- design process steps-->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
                        <li role="presentation" class="active"><a href="#discover" aria-controls="discover" role="tab"
                                data-toggle="tab"><i class="fa fa-search" aria-hidden="true"></i>
                                <p>Discover</p>
                            </a></li>
                        <li role="presentation"><a href="#strategy" aria-controls="strategy" role="tab"
                                data-toggle="tab"><i class="fa fa-send-o" aria-hidden="true"></i>
                                <p>Strategy</p>
                            </a></li>
                        <li role="presentation"><a href="#optimization" aria-controls="optimization" role="tab"
                                data-toggle="tab"><i class="fa fa-qrcode" aria-hidden="true"></i>
                                <p>Optimization</p>
                            </a></li>
                        <li role="presentation"><a href="#content" aria-controls="content" role="tab"
                                data-toggle="tab"><i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                <p>Content</p>
                            </a></li>
                        <li role="presentation"><a href="#reporting" aria-controls="reporting" role="tab"
                                data-toggle="tab"><i class="fa fa-clipboard" aria-hidden="true"></i>
                                <p>Reporting</p>
                            </a></li>
                    </ul>
                    <!-- end design process steps-->
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="discover">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Discovery</h3>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincid unt ut laoreet dolore magna aliquam erat volutpat</p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="strategy">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Strategy</h3>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincid unt ut laoreet dolore magna aliquam erat volutpat</p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="optimization">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Optimization</h3>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincid unt ut laoreet dolore magna aliquam erat volutpat</p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="content">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Content</h3>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincid unt ut laoreet dolore magna aliquam erat volutpat</p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="reporting">
                            <div class="design-process-content">
                                <h3>Reporting</h3>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

</div>
@endsection