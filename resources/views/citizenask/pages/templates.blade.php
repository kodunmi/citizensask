@extends('citizenask.layout.master')
@section('meta-information')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('title')
    Citizens Ask | Templates
@endsection
@section('content')
<div class="page--header pt--60 pb--60 text-center" data-bg-img="//citizenask/img/page-header-img/bg.jpg"
    data-overlay="0.85">
    <div class="container">
        <div class="title">
            <h2 class="h1 text-white">
                @if (Route::currentRouteName() == 'templates')
                All Templates
                @else
                    @foreach ($templates as $tmp)
                        @if ($loop->first)
                            {{$tmp->category->name}}
                        @endif   
                    @endforeach 
                @endif 
            </h2>
        </div>

        <ul class="breadcrumb text-gray ff--primary">
            <li><a href="home-1.html" class="btn-link">Home</a></li>
            <li class="active"><span class="text-primary">Templates</span></li>
        </ul>
    </div>
</div>
<section class="page--wrapper pt--80 pb--20">
    <div class="container">
        <div class="row">
            <!-- Main Content Start -->
            <div class="main--content col-md-12 pb--60">
                <div class="main--content-inner">
                    <!-- Filter Nav Start -->
                    <div class="filter--nav pb--30 clearfix">
                        <div class="filter--link float--left">
                        <h2 class="h4">All Templates : {{$templates->total()}}</h2>
                        </div>

                        <div class="filter--options float--right">
                            <label>
                                <span class="fs--14 ff--primary fw--500 text-darker">Show By :</span>

                                <select  class="form-control form-sm" id="category">
                                    <option value="">select</option>
                                    <option {{Route::currentRouteName() == 'templates' ? 'selected':''}} value="all">All Templates</option>
                                    @foreach ($cats as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach  
                                </select>
                            </label>
                        </div>
                    </div>
                    <!-- Filter Nav End -->

                    <!-- Box Items Start -->
                    <div class="box--items">
                        @if ($templates->isEmpty())
                        <h1>No Available Template</h1>
                        @else
                        <div class="row gutter--15 AdjustRow">
                            @foreach ($templates as $template)
                            <div class="col-md-3 col-xs-6 col-xxs-12 space">
                                <div class="box--item text-center">
                                    <!-- Box Item Start -->
                                    <div class="img-wrapper">
                                        <img src="/admin/images/templates/medium/{{$template->image}}" alt="Atul Prajapati">
                                        <h2>
                                            <a href="/template/show/{{$template->id}}">
                                                <button type="button" class="btn btn-white">
                                                    View This Template
                                                </button>
                                            </a>
                                            @auth
                                            <a href="/template/download/{{$template->id}}" >
                                                <button type="button" class="btn btn-white">
                                                    Download Template
                                                </button>
                                            </a>
                                            @endauth
                                            @guest
                                            <a href="{{route('login')}}" data-toggle="modal" data-overlay="0.1">
                                                <button type="button" class="btn btn-white">
                                                    Login To Download
                                                </button>
                                            </a>
                                            @endguest

                                        </h2>
                                        <ul>
                                            <li class="social-buttons">
                                                <a href="https://www.facebook.com/sharer/sharer.php?u=http://127.0.0.1:8000/template/show/{{$template->id}}" target="_blank">
                                                    <i class="fa fa-facebook-square"></i>
                                                </a>
                                            </li>
                                            <li class="social-buttons">
                                                <a href="https://twitter.com/share?text={{$template->title}}&url=http://127.0.0.1:8000/template/show/{{$template->id}}&hashtags=citizenask,youngstars,weNeedToAsk&via=youngstarsfoundation">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- Box Item End -->
                                    <div>
                                        <p class="description">{!!str_limit($template->title, 100)!!}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @endif

                    </div>
                    <!-- Box Items End -->

                    <!-- Page Count Start -->
                    <div class="page--count pt--30">
                        <label class="ff--primary fs--14 fw--500 text-darker">
                            
                            {{$templates->links()}}
                        </label>
                    </div>
                    <!-- Page Count End -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Page Wrapper End -->

<!-- Page Header End -->
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $("#category").change(function(){
            var selectedCat = $(this).children("option:selected").val();
            if (selectedCat == "all") {
                document.location.href = '/templates';
            } else {
                document.location.href = "/templates/category/"+selectedCat;
            }
            
               
        //     alert("You have selected the country - " + selectedCat);
        //     $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        // $.ajax({
        //     type: 'POST',
        //     url: '/templates/category/'+selectedCat,
        //     dataType: 'json',
        //     success: function(data) {
        //         console.log(data.cats);
        //     },
        //     error: function() {
        //        alert('failed');
        //     }
        // })
        });
    });
</script>
@endsection
