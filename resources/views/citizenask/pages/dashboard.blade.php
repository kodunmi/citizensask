@extends('citizenask.layout.master')
@section('title')
Citizens Ask | Dashboard
@endsection
@section('meta-information')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection
@section('content')
<div class="cover--header pt--80 text-center" data-bg-img="/citizenask/img/cover-header-img/bg-01.jpg"
    data-overlay="0.6" data-overlay-color="white">
    <div class="container">
        <div class="cover--avatar online" data-overlay="0.3" data-overlay-color="primary">
            @if (empty(auth()->user()->profile_image))
                <img src="https://www.gravatar.com/avatar/{{hash('md5',auth()->user()->email)}}?s=80&d=wavatar" class="profile-circle"/>
            @else
                <img src="/users/images/large/{{auth()->user()->profile_image}}" alt="" class="profile-circle"/>
            @endif
        </div>
        <div class="cover--user-name">
            <h2 class="h3 fw--600">{{auth()->user()->name}}</h2>
        </div>
        <div class="cover--user-activity">
            <p><i class="fa mr--8 fa-clock-o"></i>Membber Since {{auth()->user()->created_at->diffForHumans()}}</p>
        </div>
        <div class="cover--user-desc fw--400 fs--18 fstyle--i text-darkest">
            <p>Hello everyone ! There are many variations of passages of Lorem Ipsum available, but the majority have
                suffered alteration in some form, by injected humour.</p>
        </div>
    </div>
</div>
<section class="page--wrapper pt--80 pb--20">
    <div class="container">
        <div class="row">
            <!-- Main Content Start -->
            <div class="main--content col-md-8 pb--60" data-trigger="stickyScroll">
                <div class="main--content-inner drop--shadow">
                    <!-- Content Nav Start -->
                    <div class="content--nav pb--30">
                        <ul class="nav ff--primary fs--14 fw--500 bg-lighter">

                            <li class="active"><a>Profile</a></li>
                            <li><a data-toggle="modal" href='#modal-id'>
                            <button type="button" class="btn btn-primary">Update Profile</button>
                             </a></li>
                        </ul>
                    </div>
                    <!-- Content Nav End -->

                    <!-- Profile Details Start -->
                    <div class="profile--details fs--14">
                        <!-- Profile Item Start -->
                        <div class="profile--item">
                            <div class="profile--heading">
                                <h3 class="h4 fw--700">
                                    <span class="mr--4">About Me</span>
                                    <i class="ml--10 text-primary fa fa-caret-right"></i>
                                </h3>
                            </div>
                            <div class="profile--info">
                                <table class="table">
                                    <tr>
                                        <th class="fw--700 text-darkest">Full Name</th>
                                        <td><a href="#" class="btn-link">{{auth()->user()->name}}</a></td>
                                    </tr>
                                    <tr>
                                        <th class="fw--700 text-darkest">Age Range</th>
                                        <td>{{auth()->user()->age_range}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- Profile Item End -->

                        <!-- Profile Item Start -->
                        <div class="profile--item">
                            <div class="profile--heading">
                                <h3 class="h4 fw--700">
                                    <span class="mr--4">Biography</span>
                                    <i class="ml--10 text-primary fa fa-caret-right"></i>
                                </h3>
                            </div>
                            <div class="profile--info">
                                <p>{{auth()->user()->about_me}}</p>
                            </div>
                        </div>
                        <!-- Profile Item End -->

                        <!-- Profile Item Start -->

                        <!-- Profile Item End -->

                        <!-- Profile Item Start -->
                        <div class="profile--item">
                            <div class="profile--heading">
                                <h3 class="h4 fw--700">
                                    <span class="mr--4">Contact</span>
                                    <i class="ml--10 text-primary fa fa-caret-right"></i>
                                </h3>
                            </div>

                            <div class="profile--info">
                                <table class="table">
                                    <tr>
                                        <th class="fw--700 text-darkest">Phone</th>
                                        <td>{{auth()->user()->phone}}</td>
                                    </tr>
                                    <tr>
                                        <th class="fw--700 text-darkest">E-mail</th>
                                        <td>{{auth()->user()->email}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="fw--700 text-darkest">Country</th>
                                        <td>{{auth()->user()->country}}</td>
                                    </tr>
                                    <tr>
                                        <th class="fw--700 text-darkest">State</th>
                                        <td>{{auth()->user()->state}}</td>
                                    </tr>
                                    <tr>
                                        <th class="fw--700 text-darkest">Local Govnt</th>
                                        <td>{{auth()->user()->local_government}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- Profile Item End -->
                    </div>
                    <!-- Profile Details End -->
                </div>
            </div>
            <!-- Main Content End -->

            <!-- Main Sidebar Start -->
            <div class="main--sidebar col-md-4 pb--60" data-trigger="stickyScroll">
                <!-- Widget Start -->
                <div class="widget">
                    <h2 class="h4 fw--700 widget--title">Share Experience</h2>
                    @include('admin.layout.error')
                    @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                    @endif
                    <!-- Buddy Finder Widget Start -->
                    <div class="buddy-finder--widget">
                        <form action="/experience" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-xs-6 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Choose Category Of Template
                                                Used</span>

                                            <select name="category_id" class="form-control form-sm" id="category3"
                                                required>
                                                @foreach ($cats as $cat)
                                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Choose The Template
                                                Used</span>

                                            <select name="template_id" class="form-control form-sm" id="template3"
                                                required>

                                            </select>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Experience Title</span>
                                            <input name="title" type="text" class="form-control form-sm" required>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-xxs-12">
                                    <div class="form-group">
                                        <label>
                                            <span class="text-darker ff--primary fw--500">Experience Body</span>

                                            <textarea name="body" class="form-control form-sm" required>

                                                </textarea>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    @auth
                                    <button type="submit" class="btn btn-primary">Share Experience</button>
                                    @endauth
                                    @guest
                                    <a href="/login"><button type="button" class="btn btn-primary">Login To Share
                                            Experience</button></a>
                                    @endguest
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Buddy Finder Widget End -->
                </div>
                <!-- Widget End -->
            </div>
            <!-- Main Sidebar End -->
        </div>
    </div>

    <div class="modal fade" id="modal-id">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Update Profile</h4>
                </div>
                <div class="modal-body">

                <form>

                      <legend>
                      <div class="alert alert-success message" style="display:none;">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          <strong>Success</strong>
                      </div>
                      </legend>

                      <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ auth()->user()->name}}" required autocomplete="name" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                        <div class="col-md-6">
                            <input id="country" type="text" class="form-control" name="country" value="{{auth()->user()->country }}" required autocomplete="country" autofocus
                            >

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="province" class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>

                        <div class="col-md-6">
                            <input id="province" type="text" class="form-control" name="state" value="{{auth()->user()->state}}" required autocomplete="state" autofocus
                            >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="region" class="col-md-4 col-form-label text-md-right">{{ __('Local Government') }}</label>

                        <div class="col-md-6">
                            <input id="region" type="text" class="form-control " name="local_government" value="{{auth()->user()->local_government }}" required autocomplete="local_g" autofocus
                            >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="age_range" class="col-md-4 col-form-label text-md-right">{{ __('Age Group') }}</label>

                        <div class="col-md-6">
                            <select id="age_range" class="form-control " name="age_range" required  autofocus>
                                <option value="10-17">10-20</option>
                                <option value="20-30">20-30</option>
                                <option value="30-40">30-40</option>
                                <option value="40-50">40-50</option>
                                <option value="50-60">50-60</option>
                                <option value="60-70">60-70</option>
                                <option value="70-80">70-80</option>
                                <option value="80-90">80-90</option>
                                <option value="90-100">90-100</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control " name="email" value="{{auth()->user()->email}}" required autocomplete="email">
                        </div>
                    </div>
                    <input type="hidden" name="id" value="{{auth()->id()}}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="submit"  >Update</button>
                    </div>
                  </form>
            </div>
        </div>
    </div>

</section>
@section('script')
<script>
$('#category3').select2();
$('#template3').select2();
$('#age_range').select2();


</script>
<script type="text/javascript">

    $("#submit").click(function(e){
        e.preventDefault();



        var name = $("input[name=name]").val();
        var country = $("input[name=country]").val();
        var state = $("input[name=state]").val();
        var local_government = $("input[name=local_government]").val();
        var age_range = $("#age_range").val();
        var email = $("input[name=email]").val();
        var id = $("input[name=id]").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({

            method:'POST',

            url:'/profile/update/'+id,

            data:{
                name:name,
                country:country,
                email:email,
                state:state,
                local_government:local_government,
                age_range:age_range
               },

           success:function(data){
                $(".message").show();
              $(".message").html(data.message);
           }

        });



	});

</script>
@endsection
@endsection
