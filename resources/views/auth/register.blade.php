@extends('citizenask.layout.master')
@section('content')
<div class="page--header pt--60 pb--60 text-center" data-bg-img="///citizenask/img/page-header-img/bg.jpg" data-overlay="0.85">
    <div class="container">
        <div class="title">
            <h2 class="h1 text-white">Login</h2>
        </div>

        <ul class="breadcrumb text-gray ff--primary">
            <li><a href="home-1.html" class="btn-link">Home</a></li>
            <li class="active"><span class="text-primary">Login</span></li>
        </ul>
    </div>
</div>
<div class="contact--section pt--80 pb--20">
    <div class="container">
        <div class="col-md-6 col-md-offset-3">
                <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') error @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') error @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="number" class="form-control @error('phone') error @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">

                                @error('phone')
                                    <span class="error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="age_range" class="col-md-4 col-form-label text-md-right">{{ __('Age Group') }}</label>

                            <div class="col-md-6">
                                <select id="age_range" type="text" class="form-control @error('age_range') error @enderror" name="age_range" value="{{ old('age_range') }}" required autocomplete="age_range" autofocus>
                                    <option value="10-17">10-20</option>
                                    <option value="20-30">20-30</option>
                                    <option value="30-40">30-40</option>
                                    <option value="40-50">40-50</option>
                                    <option value="50-60">50-60</option>
                                    <option value="60-70">60-70</option>
                                    <option value="70-80">70-80</option>
                                    <option value="80-90">80-90</option>
                                    <option value="90-100">90-100</option>
                                </select>

                                @error('age_range')
                                    <span class="error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                            <div class="col-md-6">
                                <input id="country" type="text" class="form-control @error('country') error @enderror" name="country" value="{{ old('country') }}" required autocomplete="country" autofocus 
                                >
                                @error('country')
                                    <span class="error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="province" class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>

                            <div class="col-md-6">
                                <input id="province" type="text" class="form-control @error('state') error @enderror" name="state" value="{{ old('state') }}" required autocomplete="state" autofocus 
                                >

                                @error('state')
                                    <span class="error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="region" class="col-md-4 col-form-label text-md-right">{{ __('Local Government') }}</label>

                            <div class="col-md-6">
                                <input id="region" type="text" class="form-control @error('local_g') error @enderror" name="local_government" value="{{ old('local_g') }}" required autocomplete="local_g" autofocus  
                                >
                                @error('local_g')
                                    <span class="error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="about_me" class="col-md-4 col-form-label text-md-right">{{ __('About Me') }}</label>

                            <div class="col-md-6">
                                <textarea id="about_me" type="password" class="form-control @error('about_me') error @enderror" name="about_me" required autocomplete="about_me">{{ old('about_me') }}</textarea>

                                @error('about_me')
                                    <span class="error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="profile" class="col-md-4 col-form-label text-md-right">{{ __('Profile Image') }}</label>

                            <div class="col-md-6">
                                <input id="profile" type="file" class="form-control @error('profile_image') error @enderror" name="profile_image" autofocus  
                                >
                                @error('local_g')
                                    <span class="error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') error @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
    </div>
    </div>
</div>
@endsection
@section('script')
{{-- <script type="text/javascript">

    var whos=null;
    function getplaces(gid,src)
    {	
        whos = src
        
    //	var  request = "http://ws.geonames.org/childrenJSON?geonameId="+gid+"&callback=getLocation&style=long";
        //var request = "http://www.geonames.org/childrenJSON?geonameId=""&callback=listPlaces&style=long";
        var request = "https://secure.geonames.org/childrenJSON?&lang=en&style=full&geonameId="+gid+"&callback=listPlaces&username=lekan2020"

        aObj = new JSONscriptRequest(request);
        aObj.buildScriptTag();
        aObj.addScriptTag();	
    }
    
    function listPlaces(jData)
    {
        counts = jData.geonames.length<jData.totalResultsCount ? jData.geonames.length : jData.totalResultsCount
        who = document.getElementById(whos)
        who.options.length = 0;
        
        if(counts)who.options[who.options.length] = new Option('Select','')
        else who.options[who.options.length] = new Option('No Data Available','NULL')
                
        for(var i=0;i<counts;i++){
            var opt = new Option(jData.geonames[i].name,jData.geonames[i].name);
            opt.setAttribute('key', jData.geonames[i].geonameId);
             who.options[who.options.length] = opt;
             
        }
           
    
        delete jData;
        jData = null		
    }
    
    window.onload = function() { getplaces(6295630,'continent'); }
</script>
<script type="text/javascript" src="/citizenask/js/jsr_class.js"></script> --}}


{{-- <script>
    // $('#country').select2();
    // $('#continent').select2();
    // $('#province').select2();
    // $('#region').select2();
    // $('#age_range').select2();

    async function getplaces( gid , src) 
        {
            let template = $("#"+src);
            //geolocation url 
            let request = "https://secure.geonames.org/childrenJSON?&lang=en&style=full&geonameId="+gid+"&username=kodunmi3030"

            "https://secure.geonames.org/childrenJSON?&lang=en&style=full&geonameId=6295630&username=kodunmi3030"
            //await the response of the fetch call
            let response = await fetch(request);
            //proceed once the first promise is resolved.
            let data = await response.json()
            //proceed only when the second promise is resolved
            console.log(data);

            for (var i = 0; i < data.totalResultsCount; i++){
                     var option = new Option(data.geonames[i].name, data.geonames[i].name);
                     option.setAttribute('key', data.geonames[i].geonameId);
                template.append(option).trigger('change');
                } 
        }
//call getData function
window.onload = function() { getplaces(6295630,'continent'); }

$('#continent').change(function(){
    var selectedContinent = $(this).children("option:selected").attr('key');
    getplaces(selectedContinent, 'country')
    $('#country').empty();
})
$('#country').change(function(){
    var selectedProvince = $(this).children("option:selected").attr('key');
    getplaces(selectedProvince, 'province')
    $('#province').empty();
})
$('#province').change(function(){
    var selectedRegion = $(this).children("option:selected").attr('key');
    getplaces(selectedRegion, 'region')
    $('#region').empty();
})


    
</script> --}}
@endsection
