@extends('citizenask.layout.master')

@section('content')
<div class="page--header pt--60 pb--60 text-center" data-bg-img="/citizenask/img/page-header-img/bg.jpg" data-overlay="0.85">
    <div class="container">
        <div class="title">
            <h2 class="h1 text-white">Login</h2>
        </div>

        <ul class="breadcrumb text-gray ff--primary">
            <li><a href="home-1.html" class="btn-link">Home</a></li>
            <li class="active"><span class="text-primary">Login</span></li>
        </ul>
    </div>
</div>
<div class="contact--section pt--80 pb--20">
    <div class="container">
        <div class="col-md-6 col-md-offset-3">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control @error('email') error @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="error-message" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('email') error @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                        <span class="error-message" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-1 ">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-6">
                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-8 offset-md-6">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Login') }}
                    </button>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                </div>
            </div>
        </form>
    </div>
    </div>
</div>
@endsection
