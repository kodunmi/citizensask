<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

//this is the admin registration route with 'admin as prefix'
//Route::redirect('/admin', '/admin/dashboard');
Route::get('/admins', function () {
    return redirect()->route('admin.dashboard');
});
Route::group(['prefix' => 'admin'], function () {
    Route::get('/register', 'SessionController@showRegistrationForm')->name('admin.register');
    Route::post('/register', 'SessionController@register')->name('admin.register.submit');
    Route::get('/login', 'SessionController@showLoginForm')->name('admin.login');
    Route::post('/login', 'SessionController@login')->name('admin.login.submit');
    Route::get('/logout','SessionController@logout')->name('admin.logout');
    Route::get('/profile','AdminController@profile')->name('admin.profile');
    Route::post('/profile/update/{id}','AdminController@profileUpdate')->name('admin.profile.update');

    //this for adding viewing and deleting category
    Route::get('/add-category', 'CategoryController@showCatForm')->name('add-cat');
    Route::post('/add-category', 'CategoryController@addCategory');
    Route::get('/view-category', 'CategoryController@viewCategory')->name('view-cat');
    Route::post('/edit-category/{id}', 'CategoryController@editCategory');
    Route::get('/delete-category/{id}', 'CategoryController@deleteCategory');
    // this for adding deleting templates
    Route::get('/add-template', 'TemplateController@showTmpForm')->name('add-tmp');
    Route::post('/add-template', 'TemplateController@addTemplate');
    Route::get('/view-template', 'TemplateController@viewTemplate')->name('view-tmp');
    Route::post('/edit-template/{id}', 'TemplateController@editTemplate');
    Route::get('/delete-template/{id}', 'TemplateController@deleteTemplate');

});

// test 
Route::get('/admin/dashboard', function () {
    return view('admin.pages.home');
})->name('admin.dashboard')->middleware('auth:admin');

//this is the pages route
Route::get('/citizenaskk', 'PageController@index')->name('citizenask');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/templates', 'PageController@templates')->name('templates');
Route::get('/template/show/{id}', 'PageController@showTemplate');
Route::get('/template/download/{template_id}', 'PageController@downloadTemplate');
Route::get('/how-to-ask', 'PageController@howToAsk')->name('howToAsk');
Route::get('/experience', 'PageController@experience')->name('experience');
Route::get('/about', 'PageController@about')->name('about');
Route::get('/dashboard', 'PageController@dashboard')->name('dashboard');
Route::get('/templates/category/{id}', 'PageController@showCategory');
//this route is for experience [to call templates belonging to categories in share experience for {ajax call}]
Route::get('/category/templates/{id}', 'PageController@getTemplates');
//this route is for updating profile
Route::post('/profile/update/{id}','PageController@updateProfile');

// this is for storing experience
Route::post('/experience', 'ExperienceController@storeExperience');
Route::get('/experience/show/{id}', 'ExperienceController@showExperience');

//thiss is for comment

Route::post('/comment/store', 'CommentController@store')->name('comment.add');
Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');

//this is for viewing a user's profile
Route::get('/user/{user_name}/{id}','PageController@viewProfile')->name('profile');



Route::get('/home', 'HomeController@index')->name('home');
